# TP2 pt. 2 : Maintien en condition opérationnelle

# I. Monitoring

## 1. Le concept

## 2. Setup

🌞 **Setup Netdata**

🌞 **Manipulation du *service* Netdata**
```
[pilou@web ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-25 12:14:42 CEST; 2min 33s ago
 Main PID: 2306 (netdata)
    Tasks: 33 (limit: 11398)
   Memory: 45.4M
   CGroup: /system.slice/netdata.service
           ├─2306 /opt/netdata/bin/srv/netdata -P /opt/netdata/var/run/netdata/netdata.pid -D
           ├─2316 /opt/netdata/bin/srv/netdata --special-spawn-server
           ├─2476 /opt/netdata/usr/libexec/netdata/plugins.d/apps.plugin 1
           └─2482 /opt/netdata/usr/libexec/netdata/plugins.d/go.d.plugin 1

Oct 25 12:14:42 web.tp2.linux [2306]: CONFIG: cannot load user config '/opt/netdata/etc/netdata/netdata.conf'. Will try>
Oct 25 12:14:42 web.tp2.linux netdata[2306]: 2021-10-25 12:14:42: netdata INFO  : MAIN : CONFIG: cannot load user confi>
Oct 25 12:14:42 web.tp2.linux [2306]: CONFIG: cannot load stock config '/opt/netdata/usr/lib/netdata/conf.d/netdata.con>
Oct 25 12:14:42 web.tp2.linux netdata[2306]: 2021-10-25 12:14:42: netdata INFO  : MAIN : CONFIG: cannot load stock conf>
Oct 25 12:14:42 web.tp2.linux [2306]: CONFIG: cannot load cloud config '/opt/netdata/var/lib/netdata/cloud.d/cloud.conf>
Oct 25 12:14:42 web.tp2.linux netdata[2306]: 2021-10-25 12:14:42: netdata INFO  : MAIN : CONFIG: cannot load cloud conf>
Oct 25 12:14:42 web.tp2.linux [2306]: Found 0 legacy dbengines, setting multidb diskspace to 256MB
Oct 25 12:14:42 web.tp2.linux netdata[2306]: 2021-10-25 12:14:42: netdata INFO  : MAIN : Found 0 legacy dbengines, sett>
Oct 25 12:14:42 web.tp2.linux [2306]: Created file '/opt/netdata/var/lib/netdata/dbengine_multihost_size' to store the >
Oct 25 12:14:42 web.tp2.linux netdata[2306]: 2021-10-25 12:14:42: netdata INFO  : MAIN : Created file '/opt/netdata/var>
[pilou@web ~]$ sudo systemctl is-enabled netdata
enabled
```
- cmd ```ss```
```
[pilou@web ~]$ sudo ss -tulpn | grep netdata
[sudo] password for pilou:
udp   UNCONN 0      0          127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=1546,fd=43))
udp   UNCONN 0      0              [::1]:8125          [::]:*    users:(("netdata",pid=1546,fd=30))
tcp   LISTEN 0      128        127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=1546,fd=45))
tcp   LISTEN 0      128          0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=1546,fd=5))
tcp   LISTEN 0      128            [::1]:8125          [::]:*    users:(("netdata",pid=1546,fd=44))
tcp   LISTEN 0      128             [::]:19999         [::]:*    users:(("netdata",pid=1546,fd=6))
```
- Je vérifie son fonctionnement sur le navigateur:
![](https://i.imgur.com/XAiwuDr.png)

🌞 **Setup Alerting**


🌞 **Config alerting**
chemin du fichier pour la ram 
```/opt/netdata/usr/lib/netdata/conf.d/health.d/ram.conf```
```
[...]
    alarm: ram_in_use
       on: system.ram
    class: Utilization
     type: System
component: Memory
       os: linux
    hosts: *
     calc: ($used - $used_ram_to_ignore) * 100 / ($used + $cached + $free + $buffers)
    units: %
    every: 10s
     warn: $this > (($status >= $WARNING)  ? (50) : (90))
     crit: $this > (($status == $CRITICAL) ? (90) : (98))
    delay: down 15m multiplier 1.5 max 1h
     info: Percentage of used RAM. \
           High RAM utilization. \
           It may affect the performance of applications. \
           If there is no swap space available, OOM Killer can start killing processes. \
           You might want to check per-process memory usage to find the top consumers.
       to: sysadmin

    alarm: ram_available
       on: mem.available
    class: Utilization
[...]
```

# II. Backup

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?             |

🖥️ **VM `backup.tp2.linux`**

## 1. Intwo bwo

## 2. Partage NFS

🌞 **Setup environnement**
```
[pilou@backup ~]$ sudo mkdir /srv/backup/web.tp2.linux
[pilou@backup ~]$ sudo mkdir /srv/backup/db.tp2.linux
```
```
[pilou@backup ~]$ sudo chmod 666 /srv/backup/
```
🌞 **Setup partage NFS**

🌞 **Setup points de montage sur `web.tp2.linux`**

