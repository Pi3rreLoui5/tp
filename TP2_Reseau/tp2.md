# TP2 : On va router des trucs
## I. ARP

1. Echange ARP

🌞Générer des requêtes ARP

**Table ARP de la machine 1**
```
[pilou@node1 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:03 [ether] on enp0s8
? (10.2.1.12) at 08:00:27:85:a9:43 [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
```
**Table ARP de la machine 2**
```
[pilou@node2 ~]$ arp -a
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.1.11) at 08:00:27:eb:4a:f0 [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:03 [ether] on enp0s8
```

- Adresse MAC de node1 : ```08:00:27:eb:4a:f0```

- Adresse MAC de node2 : ```08:00:27:85:a9:43``` 


**La preuve que ce sont bien les adresses MAC correspondantes !**
```
[pilou@node1 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:03 [ether] on enp0s8
? (10.2.1.12) at 08:00:27:85:a9:43 [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
```

```
[pilou@node2 ~]$ ip a
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:85:a9:43 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe85:a943/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

2. Analyse de trames

🌞Analyse de trames

***Utilisez la commande tcpdump pour réaliser une capture de trame
videz vos tables ARP, sur les deux machines, puis effectuez un ping***


Sur la 3éme VM qui analyse le trafic entre node1 et node2

**cmd =** ```sudo tcpdump -i enp0s8 port not 22 -w tp2_arp.pcap```

Puis on vide les tables ARP de node1 et node2

**cmd =** ```sudo ip -s -s neigh flush all```


| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Requête ARP | `node1` `AA:BB:CC:DD:EE` | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `node2` `AA:AA:AA:AA:AA` | `node1` `AA:BB:CC:DD:EE`   |
| ...   | ...         | ...                      | ...                        |

![](https://i.imgur.com/7CwcIGI.png)


## II. Routage

### 1. Mise en place du routage

🌞Activer le routage sur le noeud router.net2.tp2

**Cmd =** ```sudo firewall-cmd --add-masquerade --zone=public --permanent```
```
[pilou@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

🌞Ajouter les routes statiques nécessaires pour que ```node1.net1.tp2``` et ```marcel.net2.tp2``` puissent se ping

```sudo ip route add <NETWORK_ADDRESS> via <IP_GATEWAY> dev <LOCAL_INTERFACE_NAME>```

**router :**
```
[pilou@router ~]$ sudo firewall-cmd  --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[pilou@router ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.254 metric 101
10.2.2.0/24 dev enp0s9 proto kernel scope link src 10.2.2.254 metric 102
```

**node1 :**
```
[pilou@node1 ~]$ ip r s
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
[pilou@node1 ~]$ sudo ip route add 10.2.2.0/24 via 10.2.1.254 dev enp0s8
[pilou@node1 ~]$ ip r s
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
10.2.2.0/24 via 10.2.1.254 dev enp0s8
```

**marcel :** 
```
[pilou@marcel ~]$ ip r s
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
[pilou@marcel ~]$ sudo ip route add 10.2.1.0/24 via 10.2.2.254 dev enp0s8
[sudo] password for pilou:
[pilou@marcel ~]$ ip r s
10.2.1.0/24 via 10.2.2.254 dev enp0s8
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```

une fois les routes en place, vérifiez avec un ping que les deux machines peuvent se joindre

**node1 :**
```
[pilou@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=0.685 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.982 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.08 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=0.836 ms
^C
--- 10.2.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3054ms
rtt min/avg/max/mdev = 0.685/0.895/1.079/0.152 ms
```


### 2. Analyse de trames
🌞Analyse des échanges ARP

- *videz les tables ARP des trois noeuds*

**cmd =** ```sudo ip neigh flush all```

- *Effectuez un ping de node1.net1.tp2 vers marcel.net2.tp2*
```
[pilou@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.07 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.953 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=0.938 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=0.913 ms
64 bytes from 10.2.2.12: icmp_seq=5 ttl=63 time=0.979 ms
64 bytes from 10.2.2.12: icmp_seq=6 ttl=63 time=0.896 ms
64 bytes from 10.2.2.12: icmp_seq=7 ttl=63 time=0.986 ms
^C
--- 10.2.2.12 ping statistics ---
7 packets transmitted, 7 received, 0% packet loss, time 6010ms
rtt min/avg/max/mdev = 0.896/0.962/1.069/0.053 ms
```

- *Regardez les tables ARP des trois noeuds*

**node1 :**
```
[pilou@node1 ~]$ arp -a
? (10.2.1.254) at 08:00:27:85:a9:43 [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:03 [ether] on enp0s8
```

**marcel :**
```
[pilou@marcel ~]$ arp -a
? (10.2.2.1) at 0a:00:27:00:00:15 [ether] on enp0s8
? (10.2.2.254) at 08:00:27:37:12:12 [ether] on enp0s8
```

**router :**
```
[pilou@router ~]$ arp -a
? (10.2.2.12) at 08:00:27:c4:d8:2f [ether] on enp0s9
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.1.1) at 0a:00:27:00:00:03 [ether] on enp0s8
? (10.2.1.11) at 08:00:27:eb:4a:f0 [ether] on enp0s8
```

- *Essayez de déduire un peu les échanges ARP qui ont eu lieu*

Les requêttes ARP se font bien entre node1 et le routeur et du router a marcel. Et les requêttes ARP ne passe pas directement entre node1 et marcel.


- *répétez l'opération précédente (vider les tables, puis ping), en lançant tcpdump sur les 3 noeuds, afin de capturer les échanges depuis les 3 points de vue
écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames*


```
1	0.000000	PcsCompu_eb:4a:f0	Broadcast	ARP	42	Who has 10.2.1.254? Tell 10.2.1.11

2	0.000280	PcsCompu_85:a9:43	PcsCompu_eb:4a:f0	ARP	60	10.2.1.254 is at 08:00:27:85:a9:43

3	0.000288	10.2.1.11	10.2.2.12	ICMP	98	Echo (ping) request  id=0x0678, seq=1/256, ttl=64 (reply in 4)

4	0.001001	10.2.2.12	10.2.1.11	ICMP	98	Echo (ping) reply    id=0x0678, seq=1/256, ttl=63 (request in 3)

11	5.062782	PcsCompu_85:a9:43	PcsCompu_eb:4a:f0	ARP	60	Who has 10.2.1.11? Tell 10.2.1.254

12	5.062795	PcsCompu_eb:4a:f0	PcsCompu_85:a9:43	ARP	42	10.2.1.11 is at 08:00:27:eb:4a:f0
```



| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `node1` `AA:BB:CC:DD:EE`  | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `marcel` `AA:AA:AA:AA:AA` | x              | `node1` `AA:BB:CC:DD:EE`   |
| ...   | ...         | ...       | ...                       |                |                            |
| ?     | Ping        | ?         | ?                         | ?              | ?                          |
| ?     | Pong        | ?         | ?                         | ?              | ?                          |

### 3. Accès internet
🌞Donnez un accès internet à vos machines

- **node1**

**cmd =**  ```sudo nano /etc/sysconfig/network```

```
GATEWAY=10.2.1.254
```


**cmd =** ```ip r s```
```
[pilou@node1 ~]$ ip r s
default via 10.2.1.254 dev enp0s8 proto static metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
```

```
[pilou@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=18.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=20.8 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=19.9 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=19.1 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=114 time=19.5 ms
^C
--- 8.8.8.8 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4007ms
rtt min/avg/max/mdev = 18.208/19.504/20.770/0.852 ms
```

**cmd =** ```sudo nano /etc/resolv.conf```
```
# Generated by NetworkManager
search auvence.co
nameserver 10.33.10.2
nameserver 10.33.10.148
nameserver 10.33.10.155
nameserver 1.1.1.1
```

```
[pilou@node1 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 43183
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             175     IN      A       216.58.214.78

;; Query time: 144 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Sep 26 20:01:22 CEST 2021
;; MSG SIZE  rcvd: 55
```

```
[pilou@node1 ~]$ ping google.com
PING google.com (216.58.214.78) 56(84) bytes of data.
64 bytes from fra15s10-in-f14.1e100.net (216.58.214.78): icmp_seq=1 ttl=114 time=17.8 ms
64 bytes from fra15s10-in-f14.1e100.net (216.58.214.78): icmp_seq=2 ttl=114 time=17.9 ms
64 bytes from fra15s10-in-f14.1e100.net (216.58.214.78): icmp_seq=3 ttl=114 time=18.0 ms
64 bytes from fra15s10-in-f14.1e100.net (216.58.214.78): icmp_seq=4 ttl=114 time=17.10 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 17.774/17.920/18.035/0.166 ms
```

- **marcel**

**cmd =** ```sudo nano /etc/sysconfig/network```

```
GATEWAY=10.2.2.254
```

**cmd =** ```ip r s```

```
[pilou@marcel ~]$ ip r s
default via 10.2.2.254 dev enp0s8 proto static metric 100
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```

```
[pilou@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=22.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=23.5 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=20.5 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=19.1 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 19.145/21.410/23.474/1.704 ms
```

**cmd =** ```sudo nano /etc/resolv.conf```

```
# Generated by NetworkManager
search net2.tp2
nameserver 89.2.0.1
nameserver 89.2.0.2
nameserver 1.1.1.1
```

```
[pilou@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 31804
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             229     IN      A       216.58.213.142

;; Query time: 21 msec
;; SERVER: 89.2.0.1#53(89.2.0.1)
;; WHEN: Sun Sep 26 20:07:37 CEST 2021
;; MSG SIZE  rcvd: 55
```

🌞Analyse de trames

| ordre | type trame  | ip source   | source                      | ip destination | destination                 |
|-------|-------------|-------------|-----------------------------|----------------|-----------------------------|
| 1     | Ping        | 10.2.1.11   | `node1` `08:00:27:eb:4a:f0` | 8.8.8.8      | `google` `08:00:27:85:a9:43`|
| 2     | Pong        | 8.8.8.8   | `google` `08:00:27:85:a9:43`| 10.2.1.11      | `node1` `08:00:27:eb:4a:f0` |


## III. DHCP

### 1. Mise en place du serveur DHCP

🌞Sur la machine node1.net1.tp2, vous installerez et configurerez un serveur DHCP (go Google "rocky linux dhcp server").

- **Dans node1**

**cmd =** ```sudo dnf install dhcp-server```

```sudo firewall-cmd --add-service=dhcp```

```sudo firewall-cmd --runtime-to-permanent```

```sudo nano /etc/dhcp/dhcpd.conf```

```
[pilou@node1 ~]$ sudo systemctl status dhcpd
● dhcpd.service - DHCPv4 Server Daemon
   Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2021-09-26 21:12:12 CEST; 33s ago
     Docs: man:dhcpd(8)
           man:dhcpd.conf(5)
 Main PID: 2126 (dhcpd)
   Status: "Dispatching packets..."
    Tasks: 1 (limit: 11398)
   Memory: 4.9M
   CGroup: /system.slice/dhcpd.service
           └─2126 /usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid

Sep 26 21:12:12 node1.net1.tp2 dhcpd[2126]: Config file: /etc/dhcp/dhcpd.conf
Sep 26 21:12:12 node1.net1.tp2 dhcpd[2126]: Database file: /var/lib/dhcpd/dhcpd.leases
Sep 26 21:12:12 node1.net1.tp2 dhcpd[2126]: PID file: /var/run/dhcpd.pid
Sep 26 21:12:12 node1.net1.tp2 dhcpd[2126]: Source compiled to use binary-leases
Sep 26 21:12:12 node1.net1.tp2 dhcpd[2126]: Wrote 0 leases to leases file.
Sep 26 21:12:12 node1.net1.tp2 dhcpd[2126]: Listening on LPF/enp0s8/08:00:27:eb:4a:f0/10.2.1.0/24
Sep 26 21:12:12 node1.net1.tp2 dhcpd[2126]: Sending on   LPF/enp0s8/08:00:27:eb:4a:f0/10.2.1.0/24
Sep 26 21:12:12 node1.net1.tp2 dhcpd[2126]: Sending on   Socket/fallback/fallback-net
Sep 26 21:12:12 node1.net1.tp2 dhcpd[2126]: Server starting service.
Sep 26 21:12:12 node1.net1.tp2 systemd[1]: Started DHCPv4 Server Daemon.
```

Mon node2 est bien connecté en DHCP

```
[pilou@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b3:ce:58 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.2/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 485sec preferred_lft 485sec
    inet6 fe80::a00:27ff:feb3:ce58/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```


🌞Améliorer la configuration du DHCP

```
[pilou@node2 ~]$ dig 8.8.8.8

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> 8.8.8.8
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 32492
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;8.8.8.8.                       IN      A

;; AUTHORITY SECTION:
.                       85231   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2021092600 1800 900 604800 86400

;; Query time: 25 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Sun Sep 26 21:32:11 CEST 2021
;; MSG SIZE  rcvd: 111
```

```
[pilou@node2 ~]$ ping google.com
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=114 time=42.5 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=2 ttl=114 time=19.1 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=3 ttl=114 time=18.2 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=4 ttl=114 time=18.4 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 18.196/24.546/42.476/10.358 ms
```



### 2. Analyse de trames
🌞Analyse de trames


- sur node2

**cmd =** ```sudo dhclient -r```

**cmd =** ```sudo dhclient```

| ordre | type trame  | ip source   | source                      | ip destination | destination                 |
|-------|-------------|-------------|-----------------------------|----------------|-----------------------------|
| 1     | DHCP Release | 10.2.1.3    | `node2` `08:00:27:b3:ce:58` | 10.2.1.11     | `node1` `08:00:27:eb:4a:f0`|
| 2     | DHCP Discover | 0.0.0.0    | `node2` `08:00:27:b3:ce:58`| 255.255.255.255 | `Broadcast` `FF:FF:FF:FF:FF` |
| 3     | DHCP Offer | 10.2.1.11     | `node1` `08:00:27:eb:4a:f0`| 10.2.1.3        | `node2` `08:00:27:b3:ce:58`|
| 4     | DHCP Request | 0.0.0.0     | `node2` `08:00:27:b3:ce:58`| 255.255.255.255| `Broadcast` `FF:FF:FF:FF:FF`|
| 5     | DHCP ACK     | 10.2.1.11   | `node1` `08:00:27:eb:4a:f0` | 10.2.1.3      | `node2` `08:00:27:b3:ce:58`|
| 6     | Requête ARP| x | `node2` `08:00:27:b3:ce:58`| x     | `Broadcast` `FF:FF:FF:FF:FF` |
| 7     | Requête ARP| x  | `node2` `08:00:27:b3:ce:58`| x     | `Broadcast` `FF:FF:FF:FF:FF` |
| 8     | Requête ARP| x  | `node1` `08:00:27:eb:4a:f0`| x     | `node2` `08:00:27:b3:ce:58` |
| 9     | Réponse ARP| x  | `node2` `08:00:27:b3:ce:58`| x     | `node1` `08:00:27:eb:4a:f0` |

## CONCLUSION