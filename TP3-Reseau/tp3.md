# TP3 : Progressons vers le réseau d'infrastructure

# I. (mini)Architecture réseau

🌞 **Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme** :

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `server1`     | `10.3.1.0`        | `255.255.255.128` | 126    | `10.3.1.126`       | `10.3.1.127`         |
| `client1`     | `10.3.1.128`        | `255.255.255.192` | 62     | `10.3.1.190`        | `10.3.1.191`          |
| `server2`     | `10.3.1.192`        | `255.255.255.240` | 14     | `10.3.1.206`        | `10.3.1.207`          |

---

🌞 **Vous remplirez aussi** au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, **le 🗃️ tableau d'adressage 🗃️ suivant :**

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.?.?/?`         | `10.3.?.?/?`         | `10.3.?.?/?`         | Carte NAT             |
| ...          | ...                  | ...                  | ...                  | `10.3.?.?/?`          |

> *N'oubliez pas de **TOUJOURS** fournir le masque quand vous écrivez une IP.*

## 2. Routeur

🖥️ **VM router.tp3**

🌞 **Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :**
```
[pilou@router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7c:25:49 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 84865sec preferred_lft 84865sec
    inet6 fe80::a00:27ff:fe7c:2549/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:27:4e:eb brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe27:4eeb/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:60:84:a4 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe60:84a4/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e7:80:2f brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.206/28 brd 10.3.1.207 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fee7:802f/64 scope link
       valid_lft forever preferred_lft forever
```

- il a un accès internet
```
[pilou@router ~]$ ping 8.8.8.8 -c 4
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=118 time=16.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=118 time=134 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=118 time=15.2 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=118 time=16.6 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 15.150/45.466/133.672/50.929 ms
```
- il a de la résolution de noms
```
[pilou@router ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 19620
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10337   IN      A       92.243.16.143

;; Query time: 7 msec
;; SERVER: 192.168.0.254#53(192.168.0.254)
;; WHEN: Mon Oct 04 14:23:30 CEST 2021
;; MSG SIZE  rcvd: 53
```
- il porte le nom `router.tp3`
```
[pilou@router ~]$ sudo nano /etc/hostname
router.tp3
```
- n'oubliez pas d'activer le routage sur la machine
```
[pilou@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
[sudo] password for pilou:
Warning: ALREADY_ENABLED: masquerade already enabled in 'public'
success
[pilou@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
Warning: ALREADY_ENABLED: masquerade
success
```

# II. Services d'infra

## 1. Serveur DHCP

🌞 **Mettre en place une machine qui fera office de serveur DHCP**

🖥️ **VM `dhcp.client1.tp3`**

```
[pilou@dhcp ~]$ ip a
[...]
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:45:31:29 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.130/26 brd 10.3.1.191 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe45:3129/64 scope link
       valid_lft forever preferred_lft forever
```
```
[pilou@dhcp ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 25039
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             166     IN      A       216.58.215.46
[...]
```
---

🌞 **Mettre en place un client dans le réseau `client1`**


🌞 **Depuis `marcel.client1.tp3`**

🖥️ **VM marcel.client1.tp3**
```
[pilou@marcel ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:5a:52:fe brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.131/26 brd 10.3.1.191 scope global dynamic noprefixroute enp0s8
       valid_lft 570sec preferred_lft 570sec
    inet6 fe80::a00:27ff:fe5a:52fe/64 scope link
       valid_lft forever preferred_lft forever
```
- prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP
```
[pilou@marcel ~]$ ip r s
default via 10.3.1.190 dev enp0s8 proto dhcp metric 100
10.3.1.128/26 dev enp0s8 proto kernel scope link src 10.3.1.131 metric 100
```
```
[pilou@marcel ~]$ dig ynov.com
[...]
;; ANSWER SECTION:
ynov.com.               9628    IN      A       92.243.16.143
[...]
```
- à l'aide de la commande `traceroute`, prouver que `marcel.client1.tp3` passe par `router.tp3` pour sortir de son réseau
```
[pilou@marcel ~]$ traceroute ynov.com
traceroute to ynov.com (92.243.16.143), 30 hops max, 60 byte packets
 1  _gateway (10.3.1.190)  0.721 ms  0.679 ms  0.628 ms
 2  10.0.2.2 (10.0.2.2)  0.605 ms  56.367 ms  56.350 ms
```

## 2. Serveur DNS

### A. Our own DNS server

### B. SETUP copain

🌞 **Mettre en place une machine qui fera office de serveur DNS**


🌞 **Tester le DNS depuis `marcel.client1.tp3`**

- définissez **manuellement** l'utilisation de votre serveur DNS
```
[pilou@marcel ~]$ cat /etc/resolv.conf
# Generated by NetworkManager

nameserver 10.3.1.2
```
- essayez une résolution de nom avec `dig`

```
[pilou@marcel ~]$ dig dns1.server1.tp3
[...]
;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.1.2
[...]
;; Query time: 1 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Mon Oct 18 00:32:59 CEST 2021
;; MSG SIZE  rcvd: 103

[pilou@marcel ~]$ dig google.com
[...]
;; ANSWER SECTION:
google.com.             300     IN      A       172.217.22.142
[...]
;; Query time: 164 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Mon Oct 18 00:29:14 CEST 2021
;; MSG SIZE  rcvd: 331
```

## 3. Get deeper (je saute cet étape)

### A. DNS forwarder

🌞 **Affiner la configuration du DNS**

### B. On revient sur la conf du DHCP

🌞 **Affiner la configuration du DHCP**

- faites en sorte que votre DHCP donne désormais l'adresse de votre serveur DNS aux clients
- créer un nouveau client `johnny.client1.tp3` qui récupère son IP, et toutes les nouvelles infos, en DHCP


# III. Services métier

## 1. Serveur Web

🌞 **Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients**

🌞 **Test test test et re-test**

- testez que votre serveur web est accessible depuis `marcel.client1.tp3`

---

## 2. Partage de fichiers (je saute cet étape)

### B. Le setup wola

🌞 **Setup d'une nouvelle machine, qui sera un serveur NFS**

🌞 **Configuration du client NFS**

🌞 **TEEEEST**

- tester que vous pouvez lire et écrire dans le dossier `/srv/nfs` depuis `web1.server2.tp3`
- vous devriez voir les modifications du côté de  `nfs1.server2.tp3` dans le dossier `/srv/nfs_share/`

# IV. Un peu de théorie : TCP et UDP

Bon bah avec tous ces services, on a de la matière pour bosser sur TCP et UDP. P'tite partie technique pure avant de conclure.

🌞 **Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP :**

- SSH
- HTTP
- DNS
- (NFS)

📁 **Captures réseau `tp3_ssh.pcap`, `tp3_http.pcap`, `tp3_dns.pcap` et `tp3_nfs.pcap`**

> **Prenez le temps** de réfléchir à pourquoi on a utilisé TCP ou UDP pour transmettre tel ou tel protocole. Réfléchissez à quoi servent chacun de ces protocoles, et de ce qu'on a besoin qu'ils réalisent.

🌞 **Expliquez-moi pourquoi je ne pose pas la question pour DHCP.**

🌞 **Capturez et mettez en évidence un *3-way handshake***

📁 **Capture réseau `tp3_3way.pcap`**

# V. El final

🌞 **Bah j'veux un schéma.**

- réalisé avec l'outil de votre choix
- un schéma clair qui représente
  - les réseaux
    - les adresses de réseau devront être visibles
  - toutes les machines, avec leurs noms
  - devront figurer les IPs de toutes les interfaces réseau du schéma
  - pour les serveurs : une indication de quel port est ouvert
- vous représenterez les host-only comme des switches
- dans le rendu, mettez moi ici à la fin :
  - le schéma
  - le 🗃️ tableau des réseaux 🗃️
  - le 🗃️ tableau d'adressage 🗃️
    - on appelle ça aussi un "plan d'adressage IP" :)

> J'vous le dis direct, un schéma moche avec Paint c'est -5 Points. Je vous recommande [draw.io](http://draw.io).

🌞 **Et j'veux des fichiers aussi, tous les fichiers de conf du DNS**

- 📁 Fichiers de zone
- 📁 Fichier de conf principal DNS `named.conf`
- faites ça à peu près propre dans le rendu, que j'ai plus qu'à cliquer pour arriver sur le fichier ce serait top
