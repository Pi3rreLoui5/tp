# TP2 pt. 1 : Gestion de service

# I. Un premier serveur web

## 1. Installation
```
[pilou@web ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
```
```
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.102.1.11
NETMASK=255.255.255.0
```
🌞 **Démarrer le service Apache**
```
[pilou@web ~]$ sudo systemctl start httpd.service
```
```
[pilou@web ~]$ sudo ss -lanp
```
```
tcp    LISTEN  0    128     *:80      *:*       users:(("httpd",pid=36879,fd=4),("httpd",pid=36878,fd=4),("httpd",pid=36877,fd=4),("httpd",pid=36875,fd=4))
```
🌞 **TEST**
- vérifier que le service est démarré
```
[pilou@web ~]$ sudo systemctl status httpd.service
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-10-06 10:25:37 CEST; 16min ago
     Docs: man:httpd.service(8)
 Main PID: 36875 (httpd)
   Status: "Total requests: 4; Idle/Busy workers 100/0;Requests/sec: 0.00416; Bytes served/sec:  35 B/sec"
    Tasks: 213 (limit: 11398)
   Memory: 25.0M
   CGroup: /system.slice/httpd.service
           ├─36875 /usr/sbin/httpd -DFOREGROUND
           ├─36876 /usr/sbin/httpd -DFOREGROUND
           ├─36877 /usr/sbin/httpd -DFOREGROUND
           ├─36878 /usr/sbin/httpd -DFOREGROUND
           └─36879 /usr/sbin/httpd -DFOREGROUND

Oct 06 10:25:37 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Oct 06 10:25:37 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Oct 06 10:25:38 web.tp2.linux httpd[36875]: Server configured, listening on: port 80
```
- vérifier qu'il est configuré pour démarrer automatiquement
```
[pilou@web ~]$ sudo systemctl is-enabled httpd.service
enabled
```
- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement
```
[pilou@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
[...]
```
- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web

![](https://i.imgur.com/kOP5Sdc.png)

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume

```[pilou@web ~]$ sudo systemctl enable httpd.service```

- prouvez avec une commande qu'actuellement, le service est paramétré pour démarrer quand la machine s'allume

```[pilou@web ~]$ sudo systemctl status httpd.service```

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache

```[pilou@web ~]$ cat /usr/lib/systemd/system/httpd.service```
```
[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```
🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé
```
[pilou@web ~]$ cat /etc/httpd/conf/httpd.conf | grep -i user
User apache
```
- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
```
[pilou@web ~]$ ps -ef
apache       860     839  0 22:37 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       861     839  0 22:37 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       862     839  0 22:37 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       863     839  0 22:37 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
- vérifiez avec un `ls -al` le dossier du site (dans `/var/www/...`)
```
[pilou@web ~]$ ls -al /var/www/
total 4
drwxr-xr-x.  4 root root   33 Oct  6 10:07 .
drwxr-xr-x. 22 root root 4096 Oct  6 10:07 ..
drwxr-xr-x.  2 root root    6 Jun 11 17:35 cgi-bin
drwxr-xr-x.  2 root root    6 Jun 11 17:35 html
```

- vérifiez que tout son contenu est accesible à l'utilisateur mentionné dans le fichier de conf

Le dossier est accessible pour tout le monde, enfin ceux qui y ont accés

🌞 **Changer l'utilisateur utilisé par Apache**
```
[pilou@web ~]$ sudo useradd -r -s /bin/nologin loutre
```
```
[...]
User loutre
Group loutre
[...]
```
```
[pilou@web ~]$ sudo systemctl restart httpd
```
```
[pilou@web ~]$ ps -ef | grep httpd
root        5344       1  0 22:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
loutre      5346    5344  0 22:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
loutre      5347    5344  0 22:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
loutre      5348    5344  0 22:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
loutre      5349    5344  0 22:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
pilou       5562    5285  0 22:54 pts/0    00:00:00 grep --color=auto httpd
```
🌞 **Faites en sorte que Apache tourne sur un autre port**
- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix
```
[pilou@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[...]
Listen 1105
[...]
```
- ouvrez un nouveau port firewall, et fermez l'ancien
```
[pilou@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
Warning: NOT_ENABLED: 80:tcp
success
[pilou@web ~]$ sudo firewall-cmd --add-port=1105/tcp --permanent
success
```
- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi
```
[pilou@web ~]$ sudo ss -lanp
tcp    LISTEN  0       128                                                          *:1105                     *:*       users:(("httpd",pid=5584,fd=4),("httpd",pid=5583,fd=4),("httpd",pid=5582,fd=4),("httpd",pid=5579,fd=4))
```
- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port
```
[pilou@web ~]$ curl localhost:1105
<!doctype html>
[...]

```

# II. Une stack web plus avancée

## 1. Intro
```
[pilou@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
IncludeOptional sites-enabled/*
```
## 2. Setup
[...]
### A. Serveur Web et NextCloud

🌞 Install du serveur Web et de NextCloud sur `web.tp2.linux`

```
[pilou@web ~]$ history
   90  sudo dnf install epel-release
   91  sudo dnf update
   92  sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
   93  sudo dnf module list php
   94  sudo dnf module enable php:remi-7.4
   95  sudo dnf module list php
   96  sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
   97  systemctl enable httpd
   98  systemctl status httpd
   99  sudo mkdir /etc/httpd/sites-available
  100  sudo mkdir /etc/httpd/sites-enabled
  101  vi /etc/httpd/conf/httpd.conf
  102  sudo nano /etc/httpd/sites-available/linux.tp2.web
  103  sudo ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/
  104  sudo mkdir -p /var/www/sub-domains/linux.tp2.web/html/
  105  timedatectl
  106  sudo vim /etc/opt/remi/php74/php.ini
  107  systemctl enable mariadb
  108  systemctl restart mariadb
  109  wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
  110  unzip nextcloud-22.2.0.zip
  111  cd nextcloud
  112  sudo mv * /var/www/sub-domains/linux.tp2.web/html/
  113  sudo chown -Rf apache.apache /var/www/sub-domains/linux.tp2.web/html/
```

### B. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

🌞 **Préparation de la base pour NextCloud**
```
[pilou@db ~]$ history
    1  sudo dnf install mariadb-server
    2  sudo systemctl enable mariadb
    3  sudo systemctl start mariadb
    4  sudo systemctl status mariadb
    5  sudo mysql -u root
    6  sudo firewall-cmd --add-port=3306/tcp --permanent
    7  sudo firewall-cmd --reload
```
🌞 **Exploration de la base de données**
```
[pilou@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 111
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.001 sec)

MariaDB [(none)]> USE nextcloud;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [nextcloud]> SHOW TABLES;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
| oc_accounts_data            |
| oc_activity                 |
| oc_activity_mq              |
| oc_addressbookchanges       |
| oc_addressbooks             |
| oc_appconfig                |
| oc_authtoken                |
| oc_bruteforce_attempts      |
            [...]
| oc_twofactor_providers      |
| oc_user_status              |
| oc_user_transfer_owner      |
| oc_users                    |
| oc_vcategory                |
| oc_vcategory_to_object      |
| oc_webauthn                 |
| oc_whats_new                |
+-----------------------------+
108 rows in set (0.001 sec)
```
### C. Finaliser l'installation de NextCloud

🌞 sur votre PC

🌞 **Exploration de la base de données**