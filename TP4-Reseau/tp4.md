# TP4 : Vers un réseau d'entreprise

# I. Dumb switch

## 1. Topologie 1
## 2. Adressage topologie 1

| Node  | IP            |
|-------|---------------|
| `pc1` | `10.1.1.1/24` |
| `pc2` | `10.1.1.2/24` |

## 3. Setup topologie 1
🌞 **Commençons simple**
```
PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.1.1.1/24
GATEWAY     : 255.255.255.0
DNS         :
MAC         : 00:50:79:66:68:00
LPORT       : 20008
RHOST:PORT  : 127.0.0.1:20009
MTU         : 1500
```
```
PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.1.1.2/24
GATEWAY     : 255.255.255.0
DNS         :
MAC         : 00:50:79:66:68:01
LPORT       : 20006
RHOST:PORT  : 127.0.0.1:20007
MTU         : 1500
```
--------------------------------------
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=2.749 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=2.109 ms
^C
```

# II. VLAN

## 2. Adressage topologie 2

| Node  | IP            | VLAN |
|-------|---------------|------|
| `pc1` | `10.1.1.1/24` | 10   |
| `pc2` | `10.1.1.2/24` | 10   |
| `pc3` | `10.1.1.3/24` | 20   |

### 3. Setup topologie 2

🌞 **Adressage**

- définissez les IPs statiques sur tous les VPCS

[...] PC1 et PC2 ne change pas
```
PC3> show ip

NAME        : PC3[1]
IP/MASK     : 10.1.1.3/24
GATEWAY     : 255.255.255.0
DNS         :
MAC         : 00:50:79:66:68:02
LPORT       : 20042
RHOST:PORT  : 127.0.0.1:20043
MTU         : 1500
```

🌞 **Configuration des VLANs**

```
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/3, Gi1/0, Gi1/1, Gi1/2
                                                Gi1/3, Gi2/0, Gi2/1, Gi2/2
                                                Gi2/3, Gi3/0, Gi3/1, Gi3/2
                                                Gi3/3
10   vlan_10                          active    Gi0/0, Gi0/1
20   vlan_20                          active    Gi0/2
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup
```

🌞 **Vérif**

- `pc1` et `pc2` doivent toujours pouvoir se ping
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=1.571 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=1.844 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=2.317 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=1.711 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=1.289 ms
```
```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=3.663 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=4.116 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=3.245 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=3.042 ms
```
- `pc3` ne ping plus personne
```
PC3> ping 10.1.1.1

host (10.1.1.1) not reachable

PC3> ping 10.1.1.2

host (10.1.1.2) not reachable
```

# III. Routing

## 2. Adressage topologie 3

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `admins`  | `10.2.2.0/24` | 12           |
| `servers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 3


🌞 **Adressage**


🌞 **Configuration des VLANs**

Je crée les deux vlan 

```
Switch>en
Switch#conf t
Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#exit
Switch(config)#vlan 12
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#vlan 13
Switch(config-vlan)#name servers
Switch(config-vlan)#exit
```

***Adressage des vlans aux ports***

```
Switch(config)#interface GigabitEthernet 0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#no shutdown
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet 0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#no shutdown
Switch(config-if)#exit
Switch(config-if)#interface GigabitEthernet 0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 12
Switch(config-if)#no shutdown
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet 1/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 13
Switch(config-if)#no shutdown
Switch(config-if)#exit
```

***Et je trunk***

```
Switch(config)# interface gigaBitEthernet0/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13

Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi0/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi0/0       1-4094

Port        Vlans allowed and active in management domain
Gi0/0       1,10-13,20

Port        Vlans in spanning tree forwarding state and not pruned
Gi0/0       1,10-13,20
```

***Configuration du routeur***
```
R1(config)#interface fastEthernet0/0.10
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet0/0.20
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet0/0.30
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
```

- Ping PC1

```
PC1> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=28.610 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=19.368 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=18.528 ms
```

- Ping Admin

```
admin> ping 10.2.2.254
84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=28.210 ms
84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=19.633 ms
```

- Ping web

```
PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=13.2 ms
64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=17.8 ms
```

On ajoute la route par defaut !

```
pc1> ip 10.1.1.1/24 10.1.1.254
admin> ip 10.2.2.1/24 10.2.2.254
[pilou@web ~]$ sudo ip route add default via 10.3.3.254 dev enp0s3

PC1> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=31.704 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=30.842 ms
84 bytes from 10.2.2.1 icmp_seq=3 ttl=63 time=32.219 ms

PC1> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=22.983 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=16.354 ms
```

# IV. NAT

## SETUP topologie 4

🌞 ***Ajout du noeud cloud***

Je rajoute le cloud a ma topographie et je configure le router en DHCP et renouvelle l'IP !

```
R1(config)#interface f1/0
R1(config-if)#ip address dhcp
R1(config-if)#no shut
```

je ping 1.1.1.1

```
R1#ping 1.1.1.1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 20/23/32 ms
```

🌞 ***Configurez le NAT***

```
R1(config)# interface fastEthernet 0/0
R1(config-if)#ip nat inside
R1(config-if)#exit
R1(config)#interface fastEthernet 1/0
R1(config-if)#ip nat outside
R1(config-if)#exit
```
🌞 ***Test***

- ***ajoutez une route par défaut (si c'est pas déjà fait)***
```
pc1> ip 10.1.1.1/24 10.1.1.254
admin> ip 10.2.2.1/24 10.2.2.254
[pilou@web ~]$ sudo ip route add default via 10.3.3.254 dev enp0s3
```
***ping vers un nom de domaine***

```
PC1> ping google.com
google.com resolved to 142.250.179.78
84 bytes from 142.250.179.78 icmp_seq=1 ttl=113 time=39.918 ms
84 bytes from 142.250.179.78 icmp_seq=2 ttl=113 time=32.744 ms
84 bytes from 142.250.179.78 icmp_seq=3 ttl=113 time=59.487 ms
```

## V. Add a building

**Topologie 5**

🌞 ***Mettre en place un serveur DHCP dans le nouveau bâtiment***

J'ai utilisé un serveur d'une ancienne VM, j'ai modifié son fichier de conf...

```
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.1.1.253  netmask 255.255.255.0 {
  range 10.1.1.0 10.1.1.252;
  option routers 10.1.1.254;
  option subnet-mask 255.255.255.0;
  option domain-name-servers 8.8.8.8;
}
```

